facultad de arquitectura	arquitectura	19020	10	40	20	20	10	0	500	475	728.8	569.9	120	3
facultad de arquitectura	cine(hist)	19024	10	40	30	10	10	0	500	475	762.4	572.9	40	5
facultad de arquitectura	cine(cs)	19024	10	40	30	10	0	10	500	475	762.4	572.9	40	5
facultad de arquitectura	diseno(hist)	19022	10	40	20	20	10	0	500	475	692.1	504.6	75	4
facultad de arquitectura	diseno(cs)	19022	10	40	20	20	0	10	500	475	692.1	504.6	75	4
facultad de arquitectura	gestion en turismo y cultura(hist)	19027	10	40	30	10	10	0	500	475	721.7	549.1	40	5
facultad de arquitectura	gestion en turismo y cultura(cs)	19027	10	40	30	10	0	10	500	475	721.7	549.1	40	5
facultad de arquitectura	teatro(requiere prueba especial)	19028	10	20	40	10	20	0	500	475	762.76	574.92	20	5
facultad de ciencias	ingenieria en estadisticas(hist)	19072	10	40	10	30	10	0	500	475	671.8	506.8	30	5
facultad de ciencias	ingenieria en estadisticas(cs)	19072	10	40	10	30	0	10	500	475	671.8	506.8	30	5
facultad de ciencias	lic. en ciencias mencion:biologia o quimica	19075	10	40	20	20	0	10	500	475	735.5	538.1	25	2
facultad de ciencias	lic. en fisica mencion:astronomia,ciencias atmosfericas o computacion cientifica	19078	10	20	15	45	0	10	500	500	659.65	510	45	5
facultad de ciencias	licenciatura en matematicas(hist)	19070	10	40	10	30	10	0	500	475	632,2	506.1	15	2
facultad de ciencias	licenciatura en matematicas(cs)	19070	10	40	10	30	10	10	500	475	632.2	506.1	15	2
facultad del mar y de recursos naturales	biologia marina	19080	10	40	15	25	0	10	500	475	691.35	502.45	50	5
facultad de ciencias economicas y administrativas	administracion hotelera y gastronomia(hist)	19086	10	40	20	20	10	0	500	475	731.6	500.8	60	5
facultad de ciencias economicas y administrativas	administracion hotelera y gastronomia(cs)	19086	10	40	20	20	0	10	500	475	731.6	500.8	60	5
facultad de ciencias economicas y administrativas	administracion publica-valparaiso	19068	10	20	35	25	10	0	500	500	723.45	574.9	75	2
facultad de ciencias economicas y administrativas	administracion publica-santiago	19005	10	20	35	25	10	0	500	500	655.6	571.45	75	2
facultad de ciencias economicas y administrativas	auditoria(hist)-valparaiso	19062	10	40	20	15	15	15	500	475	725.45	509.85	100	5
facultad de ciencias economicas y administrativas	auditoria(cs)-valparaiso	19062	10	40	20	15	0	15	500	475	725.45	509.65	100	5
facultad de ciencias economicas y administrativas	auditoria(hist)-santiago	19093	10	40	20	15	15	0	500	475	729.65	500.55	60	5
facultad de ciencias economicas y administrativas	auditoria(cs)-santiago	19093	10	40	20	15	0	15	500	475	729.65	500.55	60	5
facultad de ciencias economicas y administrativas	auditoria vespertino(hist)-valparaiso	19069	10	40	20	15	15	0	500	475	700.15	504.55	20	0
facultad de ciencias economicas y administrativas	auditoria vespertino(cs)-valparaiso	19069	10	40	20	15	0	15	500	475	700.15	504.55	20	0
facultad de ciencias economicas y administrativas	ingenieria comercial-vina del mar	19060	10	40	10	30	10	0	500	500	705.2	578.1	150	20
facultad de ciencias economicas y administrativas	ingenieria comercial-santiago	19095	10	40	10	30	10	0	500	500	647.3	500.9	125	12
facultad de ciencias economicas y administrativas	ingenieria en informacion y control de gestion(hist)	19083	10	40	10	30	10	0	500	475	722.8	501.8	50	5
facultad de ciencias economicas y administrativas	ingenieria en informacion y control de gestion(cs)	19083	10	40	10	30	0	10	500	475	722.8	501.8	50	5
facultad de ciencias economicas y administrativas	ingenieria en negocios internacionales(hist)-vina del mar	19088	10	30	20	30	10	0	500	475	693.6	529.2	80	5
facultad de ciencias economicas y administrativas	ingenieria en negocios internacionales(cs)-vina del mar	19088	10	30	20	30	0	10	500	475	693.6	529.2	80	5
facultad de ciencias economicas y administrativas	ingenieria en negocios internacionales(hist)-santiago	19089	10	30	20	30	10	0	500	475	694.7	511.8	35	5
facultad de ciencias economicas y administrativas	ingenieria en negocios internacionales(cs)-santiago	19089	10	30	20	30	0	10	500	475	694.7	511.8	35	5
facultad de derecho y ciencias sociales	derecho	19030	10	25	20	20	25	0	500	500	737.3	628.9	135	5
facultad de derecho y ciencias sociales	trabajo social	19031	20	30	20	20	10	0	500	475	700	556.4	78	2
facultad de farmacia	quimica y farmacia	19090	10	40	20	20	0	10	500	500	760.8	620.5	60	2
facultad de farmacia	nutricion y dietetica	19091	10	40	20	20	0	10	500	500	770.4	608.9	60	2
facultad de humanidades	pedagogia en filosofia	19010	10	40	20	10	20	0	500	500	718.9	505.7	30	2
facultad de humanidades	pedagogia en historia y ciencias sociales	19011	10	40	20	10	20	0	500	500	741.9	552.6	70	3
facultad de humanidades	pedagogia en musica	19015	10	40	25	15	10	0	500	500	619.6	529.95	40	2
facultad de humanidades	sociologia	19012	10	40	20	15	15	0	500	475	736.35	546.05	75	2
facultad de ingenieria	ingenieria ambiental	19074	10	40	10	30	0	10	500	500	740.7	547.7	55	4
facultad de ingenieria	ingenieria civil(hist)	19021	10	20	15	45	10	0	500	500	659.1	523.05	90	3
facultad de ingenieria	ingenieria civil(cs)	19021	10	20	15	45	0	10	500	500	659.1	523.05	90	3
facultad de ingenieria	ingenieria civil ambiental	19018	10	20	10	45	0	15	500	500	o	0	35	2
facultad de ingenieria	ingenieria civil biomedica(hist)	19076	10	30	10	40	10	0	500	500	749,8	584	60	2
facultad de ingenieria	ingenieria civil biomedica(cs)	19076	10	30	10	40	0	10	500	500	749.8	584	60	2
facultad de ingenieria	ingenieria civil industrial(hist)-valparaiso	19052	10	30	10	40	10	0	500	500	690.7	582.2	110	10
facultad de ingenieria	ingenieria civil industrial(cs)-valparaiso	19052	10	30	10	40	0	10	500	500	690.7	582.2	110	10
facultad de ingenieria	ingenieria civil industrial(hist)-santiago	19094	10	30	10	40	10	0	500	500	691	563.1	90	10
facultad de ingenieria	ingenieria civil industrial(cs)-santiago	19094	10	30	10	40	0	10	500	500	691	563.1	90	10
facultad de ingenieria	ingenieria civil informatica(hist)	19085	10	30	10	40	10	0	500	500	651.7	541.6	75	5
facultad de ingenieria	ingenieria civil informatica(cs)	19085	10	30	10	40	0	10	500	500	651.7	541.6	75	5
facultad de ingenieria	ingenieria civil matematica	19019	10	20	10	45	0	15	500	500	0	0	15	2
facultad de ingenieria	ingenieria civil oceanica(hist)	19026	10	40	10	30	10	0	500	475	629.6	501.2	30	2
facultad de ingenieria	ingenieria civil oceanica(cs)	19026	10	40	10	30	0	10	500	475	629.6	501.2	30	2
facultad de ingenieria	ingenieria en construccion(hist)	19026	10	25	10	45	10	0	500	475	674.55	544.1	80	5
facultad de ingenieria	ingenieria en construccion(cs)	19026	10	25	10	45	0	10	500	475	674.55	544.1	80	5
facultad de medicina	educacion parvularia(hist)	19037	10	40	30	10	10	0	500	500	652.1	530.2	35	2
facultad de medicina	educacion parvularia(cs)	19037	10	40	30	10	0	10	500	500	652.1	530.2	35	2
facultad de medicina	enfermeria-renaca	19041	10	40	25	15	0	10	500	500	773.1	681.4	75	3
facultad de medicina	enfermeria-san felipe	19042	10	40	25	15	0	10	500	500	729.85	645.8	40	2
facultad de medicina	fonoaudiologia-renaca	19046	10	40	30	10	0	10	500	500	729.7	633.7	45	2
facultad de medicina	fonoaudiologia-san felipe	19044	10	40	30	10	0	10	500	500	664.6	566.1	40	2
facultad de medicina	kinesiologia	19043	10	30	20	20	0	20	500	500	745.95	602.6	80	2
facultad de medicina	medicina-renaca	19040	10	30	20	20	0	20	600	600	815.9	762.5	72	2
facultad de medicina	medicina-san felipe	19039	10	30	20	20	0	20	600	600	764.1	751.3	42	2
facultad de medicina	obstetricia y puericultura-renaca	19047	10	40	30	10	0	10	500	500	778.1	691.1	75	2
facultad de medicina	obstetricia y puericultura-san felipe	19036	10	40	30	10	0	10	500	500	723.5	670.2	25	2
facultad de medicina	psicologia	19045	10	30	30	20	10	0	500	500	700.2	623.8	80	3
facultad de medicina	tecnologia medica-renaca	19049	10	40	25	15	0	10	500	500	756	697.2	55	2
facultad de medicina	tecnologia medica-san felipe	19048	10	40	25	15	0	10	500	500	819.15	649.1	40	2
facultad de odontologia	odontologia	19050	10	40	20	20	0	10	500	500	762.1	690.5	71	2
